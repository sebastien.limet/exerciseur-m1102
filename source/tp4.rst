TP4 - Les Chaines et les listes
*******************************


Liste des mots
--------------

Écrire une fonction qui prend en paramètres une chaîne de caractères contenant un texte et qui retourne la liste des mots qui se trouvent dans cette chaîne.
Pour cette fonction, un mot commence par un caractère alphabétique et s'arrête au premier caractère non alphabétique. La méthode ``isalpha()`` permet de tester si une chaîne ne contient que des caractères alphabétiques.

.. easypython:: /exercices/tp4/exo-liste-de-mots.py
   :language: python
   :uuid: 1231313

Liste occurrences d'une chaine:
-------------------------------

Écrire une fonction qui prend en paramètres deux chaînes et qui retourne la liste des indices où la première chaîne apparaît dans la seconde. Par exemple, si on recherche ``"le"`` dans  ``"le lundi, c'est le premier jour de la semaine"``, la fonction doit retourner la liste ``[0,16]`` car le mot ``"le"`` apparaît aux positions 0 et 16 de la chaîne considérée.

.. easypython:: /exercices/tp4/exo-liste-occurrences-chaine.py
   :language: python
   :uuid: 1231313

Le crible d'Ératosthène:
========================

Dans cet exercice nous allons implémenter un algorithme qui date du
IIIe siècle avant notre ère. Il s'agit d'un algorithme permettant de
trouver les nombres premiers dans la liste des ``N`` premiers
entiers. L'algorithme est le suivant. On construit une liste de ``N+1``
booléens tous à ``True`` sauf les deux premiers (0 et 1 ne sont pas
des nombres premiers par définition). Ensuite, pour chaque entier ``x``
si le booléen qui porte son indice est ``True``, il est répertorié
comme un nombre premier et tous ses multiples sont mis à ``False``.
Vous pouvez consulter la page https://fr.wikipedia.org/wiki/Crible_d'Ératosthène pour plus de détails.

Pour écrire cet algorithme en Python, nous allons implémenter
plusieurs fonctions

Initialisation
--------------
Écrire une fonction Python qui initialise une liste de ``N+1`` booléens tous à ``True`` sauf les deux premiers.

.. easypython:: /exercices/tp4/premier-init.py
   :language: python
   :uuid: 1231313

Multiples
---------
Écrire une fonction Python qui pour un entier  ``x`` et une liste met à ``False`` tous les booléens d'indice multiple de 
  ``x`` (sauf celui de ``x`` lui-même).

  .. easypython:: /exercices/tp4/premier-multiples.py
   :language: python
   :uuid: 1231313

Crible
------
Écrire une fonction qui implémente le crible pour les ``N`` premiers entiers. **Attention** vous devez recopier les deux fonctions précédentes dans le cadre réponse pour les utiliser dans la fonction de cet exercice.

.. easypython:: /exercices/tp4/premier-eratosthene.py
   :language: python
   :uuid: 1231313


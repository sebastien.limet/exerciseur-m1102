SDD - Exercices pour jeunes novices
************************************

.. toctree::
   :maxdepth: 2
   :numbered:
   :glob:
   
   exercices/sdd1/index*


SDD - Exercices pour padawans
********************************


.. toctree::
   :maxdepth: 2
   :numbered:
   :glob:
   
   exercices/sdd2/index*




entrees_visibles = [("samedi",),
                    ("bonjour",)
]
entrees_invisibles = [
        ("encore",),
        ("toujours",),
        ("lundi",),
        ("simili",),
        ("ios",),
]

@solution
def consonnesVoyelles(mot) :
    """
    détermine si un mot contient plus de voyelles que de consonnes 
    Paramètre : mot le mot à examiner
    Résultat : res -1 plus de consonnes, 1 plus de voyelle et 0 egalité
    """
    cpt = 0
    for lettre in mot :
        if lettre in "aeiouy":
            cpt = cpt -1
        else :
            cpt = cpt +1
    if cpt < 0:
        res = -1
    elif cpt > 0:
        res =1
    else :
        res = 0
    return res


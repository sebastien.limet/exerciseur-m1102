entrees_visibles = [(7,"samedi",),
                    (6, "lundi",)
]
entrees_invisibles = [
        (8, "samedi",),
        (6, "dimanche",),
        (7, "lundi",),
        (10,"samedi",),
        (11, "dimanche",),
]

@solution
def dejeunerOuPas(heureLeve, jourSemaine) :
    """
    Détermine en fonction du jour de la semaine et de l'heure de lever
    quel est le déjeuner pris : rien ou café ou complet
    paramètres : heureLeve : heure de lever, jourSemaine : jour de la semaine 
    résultat : res complet ou cafe ou rien 
    """
    # on détermine le temps restant 
    if jourSemaine == " samedi " :
        tempsDej = 9 - heureLeve
    elif jourSemaine == " dimanche " :
        tempsDej =11 - heureLeve
    else :
        tempsDej = 8 - heureLeve

    # on détermine le type de déjeuner
    if tempsDej > 1.5:
        res = " complet "
    elif tempsDej > 0.5:
        res = " cafe "
    else :
        res = " rien "
    return res 

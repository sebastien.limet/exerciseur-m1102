trois_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Dominique' : {'Vélo'},
    'Claude' : {'Lecture', 'Tricot', 'Boxe'}
    }

autre = {
    'a' : {'1', '2', '3'},
    'b' : {'4'},
    'a' : {'2', '3', '4'}
    }
    
    
entrees_visibles = [ trois_amis ]
entrees_invisibles = [ autre ]


@solution
def ensemble_activite(amis):
    res = set()
    for e in amis.values():
        res|=e
    return res

#  for p in entrees_visibles+entrees_invisibles:
    #  print(ensemble_activite(p))



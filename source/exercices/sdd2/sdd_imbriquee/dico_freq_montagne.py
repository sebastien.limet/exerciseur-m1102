afrique = [ ('Mont Kenya', 5199), ('Kilimandjaro', 5892), ('Mont Cameroun', 4040), ('Mont Cameroun', 4040)]
asie = [('Khunyang Chhish', 7823), ('Annapurna I', 8091), ('Everest', 8848), ('K2', 8611)]

a=[('a', 1), ('ac', 6000), ('ad', 2), ('ae', 12), ('af', -18)]
r=[('ab',100), ('b',2), ('cb',32), ('bd',8), ('abcd',16), ('ab',4)]


entrees_visibles = [ afrique, asie ]
entrees_invisibles = [a, r]


@solution
def index_alphabetique(liste):
    res = {}
    for nom,_ in liste:
        res[nom[0]] = res.get(nom[0],0) + 1
    return res

#  for e in entrees_visibles+entrees_invisibles:
    #  print(index_alphabetique(e))



.. image:: https://www.erenumerique.fr/wp-content/uploads/2019/01/poudlard-harry-potter.jpg
   :align: right
   :height: 6em


A l'école de Poudlard
---------------------------------

L'école de Poudlard accueille chaque année des élèves sorciers, répartis en plusieurs maisons.

On décide de représenter un élève par un dictionnaire, et on représente la *promotion* d'une année par une liste de dictionnaires.
Par exemple : 

.. code-block:: python

   adrian = { "nom": "Adrian", "courage": 9,"malice" : 10, "maison": "Serpentar"}
   hermione = { "nom": "Hermione",  "courage": 7, "malice" : 6, "maison":"Griffondor"}
   luna = { "nom": "Luna", "courage": 2, "malice" : 2, "maison":"Serdaigle"}
   marcus = { "nom": "Marcus", "courage": 6, "malice" : 10, "maison":"Serpentar"}
   lavande = { "nom": "Lavande", "courage": 10, "malice" : 6, "maison":"Griffondor"}

   promotion_2019 =  [adrian, hermione, luna, marcus, lavande]



.. image:: https://vignette.wikia.nocookie.net/pottermore-blabla/images/0/04/Blason_Serdaigle.png/revision/latest?cb=20121029223814&path-prefix=fr
   :align: right
   :height: 6em
   
   
**Question 1.** Écrire une fonction ``nombre_eleves`` qui prend une *promotion* en paramètre ainsi que le nom d'une maison, et qui renvoie le nombre l'élèves dans cette maison.

.. easypython:: compte_poudlard.py
   :language: python
   :uuid: 1231313



.. image:: https://www.cdiscount.com/pdt2/7/5/0/1/700x700/auc5055789209750/rw/blason-en-carton-maison-gryffondor-harry-potter-61.jpg
   :align: left
   :height: 6em
   

**Question 2.** Écrire une fonction ``contient_courageux`` qui prend une  *promotion* en paramètre ainsi que le nom d'une maison, et qui indique si la promotion compte au moins un élève courageux (courage>8) appartenant à la maison indiquée.

Par exemple, avec la *promotion 2019*, la maison *Griffondor* compte 2 étudiant alors que la maison *Poufsouffle* n'en compte aucun.


.. easypython:: contient_poudlard.py
   :language: python
   :uuid: 1231313
   
   
**Question 3.** Écrire une fonction ``le_moins_malin`` qui prend une *promotion* en paramètre ainsi que le nom d'une maison, et qui indique le nom de l'élève le moins main (celui avec la malice la plus faible) de cette maison.

.. easypython:: min_poudlard.py
   :language: python
   :uuid: 1231313


.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Blason_Poudlard.svg/545px-Blason_Poudlard.svg.png 
   :align: left
   :height: 6em


**Question 4.** Écrire une fonction ``tri_par_maison`` qui prend une promotion en paramètre et qui renvoie un dictionnaire dont les clefs sont les noms des maisons, et les valeurs l'ensemble des noms des élèves de cette maison.

.. easypython:: dico_freq_poudlard.py
   :language: python
   :uuid: 1231313


.. image:: https://pa1.narvii.com/6407/0597b8a7ccde0dfd063343f3af9a369ab9963e8f_hq.gif
   :align: left
   :height: 6em
   

**Question 5.** Écrire une fonction ``meilleur_maison`` qui prend une promotion en paramètre et qui renvoie le nom de la maison qui compte le plus grand nombre d'élèves.

.. easypython:: max_poudlard.py
   :language: python
   :uuid: 1231313



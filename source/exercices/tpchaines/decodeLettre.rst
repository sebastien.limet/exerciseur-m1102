:orphan:

Décodage d'une lettre
---------------------

Écrire une fonction *decodeLettre* qui retourne la lettre
  correspondant à un code en fonction d'une clé donnée.

.. easypython:: /exercices/tpchaines/decodeLettre.py
   :language: python
   :uuid: 1231313

entrees_visibles = [('n','nbvcxwmlkjhgfdsqpoiuy treza'),
                    ('o','nbvcxwmlkjhgfdsqpoiuy treza'),
                    ('b','nbvcxwmlkjhgfdsqpoiuy treza')
]
entrees_invisibles = [
        ('h','nbvcxwmlkjhgfdsqpoiuy treza'),
        ('a','nbvcxwmlkjhgfdsqpoiuy treza'),
        ('x','poirezuy tnwmlkjhgfbvcxdsqa')
]

@solution
def codeLettre(lettre, cle):
    res=-1
    for i in range(len(cle)):
        if cle[i]==lettre:
            res= i+1 + i//9
    return res

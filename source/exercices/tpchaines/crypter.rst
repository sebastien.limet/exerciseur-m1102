:orphan:

Cryptage d'un texte
-------------------

En utilisant la fonction *codeLettre*, écrire une fonction *crypter* qui
permet de decrypter un texte suivant une clé donnée.
**ATTENTION** n'oubliez pas de recopier le code de la fonction *codeLettre*

.. easypython:: /exercices/tpchaines/crypter.py
   :language: python
   :uuid: 1231313

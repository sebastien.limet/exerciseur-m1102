:orphan:

Exercice 5.3
------------

On a deux listes triées dans l'ordre croissant et on voudrait créer une troisième liste
qui est la fusion des deux premières et qui est elle aussi triée.
\'Ecrire une fonction qui résoud ce problème.

.. easypython:: /exercices/tp5/fusionListesTriees.py
   :language: python
   :uuid: 1231313

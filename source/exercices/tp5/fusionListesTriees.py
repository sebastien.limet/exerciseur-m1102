entrees_visibles = [([1,3,5,9],[2,3,4]),
                    ([1,2,3,4,15,18,25],[7,9,18,18,35]),
                    ([1,4,5,7],[2,3,6,9,11,14,15,27,35])
]
entrees_invisibles = [
        ([],[1,2,3,4,5,6]),
        ([2,3,4], [1,3,5,9]),
        ([7,9,18,18,35], [1,2,3,4,15,18,25],),
        ([2,3,6,9,11,14,15,27,35],[1,4,5,7])
]

@solution
def fusionListesTriees(liste1,liste2):
    """
    paramètres: deux listes de nombres TRIEES
    résultat: une liste (de nombres) qui est la fusion des deux premières et qui est elle aussi triée.
    """ 
    nouvelleListe=[]
    i=0
    j=0
    while i<len(liste1) and j<len(liste2):
      if liste1[i]>liste2[j]:
        nouvelleListe.append(liste2[j])
        j+=1
      else:
        nouvelleListe.append(liste1[i])
        i+=1
    # On complète avec la fin de la plus grande des deux listes
    while i<len(liste1):
      nouvelleListe.append(liste1[i])
      i+=1
    while j<len(liste2):
      nouvelleListe.append(liste2[j])
      j+=1
    return nouvelleListe

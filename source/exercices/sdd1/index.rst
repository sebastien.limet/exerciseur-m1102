.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Echauffement
========================

.. toctree::
   :maxdepth: 1
   :glob:
   
   elementaire/*



Des conditionnelles
========================

.. toctree::
   :maxdepth: 1
   :glob:
   
   conditionnelles/*
   


Fonctions de cumul
========================

.. toctree::
   :maxdepth: 2
   :glob:
   
   cumuls/*


Recherches d'extremums
===========================

.. toctree::
   :maxdepth: 2
   :glob:
   
   extremums/*



Fonctions de vérification
===========================

.. toctree::
   :maxdepth: 1
   :glob:
   
   verifications/*


Avec une boucle while
===========================

.. toctree::
   :maxdepth: 1
   :glob:
   
   while/*

entrees_visibles = [(["coucou", "tout", "le", "monde"],),
                    (["Tu", "ne", "sais", "rien", "John", "Snow"],),  
                    (["J'aime", "l", "informatique", "à", "l", "IUT", "d", "Orléans"],),                
]
entrees_invisibles = [
                    (["aaaa", "bb", "c", "ddd", "e","ff"],), 
                    (["aaaa", "bb", "c", "ddddd", "e","ff"],),                  
                    (["aaaa", "bb", "c", "ddd", "e","fffff"],),
                    (["aaa", "bbbb", "c", "ddd", "e","ffff", "gg"],),
]

@solution
def le_mot_le_plus_long(liste):
   return max(liste, key=len)

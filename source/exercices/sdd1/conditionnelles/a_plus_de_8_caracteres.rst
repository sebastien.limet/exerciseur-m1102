Plus de 8 caractères ?
------------------------


Écrire une fonction ``a_plus_de_8_caracteres()`` qui prend en paramètre une chaîne de caractères et qui indique si cette chaîne contient plus de 8 caractères.

.. easypython:: a_plus_de_8_caracteres.py
   :language: python
   :uuid: 1231313




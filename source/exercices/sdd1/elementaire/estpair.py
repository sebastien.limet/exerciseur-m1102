entrees_visibles=[
        (2,),
        (42,),
        (15,),
        (-8,),
]
entrees_invisibles=[(x,) for x in range(-10, 11, 1)]

@solution
def estpair(n):
  return n%2==0


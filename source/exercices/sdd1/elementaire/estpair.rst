Parité d'un nombre entier
-------------------------------

Écrire une fonction ``estpair()`` qui prend en paramètre un nombre entier et qui indique si ce nombre est pair.


.. easypython:: estpair.py
   :language: python
   :uuid: 1231313


.. image:: ../../../_static/defi.jpeg
   :height: 3em
   :align: left

Pouvez-vous écrire cette fonction sans le mot clef ``if`` ?

Combien de lettres ?
-------------------------------

Écrire une fonction ``nombre_de_lettres()`` qui prend en paramètre une chaîne de caratères entier et qui indique combien de caractères elle contient.


.. easypython:: nombre_de_lettres.py
   :language: python
   :uuid: 1231313



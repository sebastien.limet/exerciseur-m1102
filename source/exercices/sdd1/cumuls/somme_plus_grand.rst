Somme des nombres plus grands que 5
---------------------------------------

Écrire une fonction ``somme_plus_grand()`` qui prend en paramètre une liste de nombres et renvoie la somme des nombres plus grands que 5.


.. easypython:: somme_plus_grand.py
   :language: python
   :uuid: 1231313


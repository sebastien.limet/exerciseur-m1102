entrees_visibles=[
        ([10, 12, 8, 20], [1, 2, 1, 4]),
        ([10, 0, 8, 20], [1, 1, 1, 1]),
        ([15, 12, 10, 16], [3, 1, 1, 4]),
]
entrees_invisibles=[
        ([1, 2, 3, 4, 5, 6, 7], [1, 2, 3, 4, 5, 6, 7]),
        ([10, 2, 30], [1, 0, 3]),
        ([2, 5, 6, 4], [1, 2, 1, 4]),
]


@solution
def moyenne_ponderee(notes, coefficients):
  return sum([n*c for (n,c) in zip(notes, coefficients)])/sum(coefficients)


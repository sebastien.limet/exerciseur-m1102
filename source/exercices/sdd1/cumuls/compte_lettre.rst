Combien de lettres ?
---------------------------------

Écrire une fonction ``compte_lettre(lettre chaine)`` qui prend en paramètre une lattre et une chaîne de caractères et qui renvoie le nombre d'occurrence de la lettre dans la chaîne.

.. easypython:: compte_lettre.py
   :language: python
   :uuid: 1231313

